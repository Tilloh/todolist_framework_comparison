let exampleTodos = [];
let realTodos = [
    {"completed" : false, "title": "Wäsche waschen"},
    {"completed" : true,  "title": "Aufräumen"},
    {"completed" : true,  "title": "Wohnung saugen"}
];	

let exampleCheck = false;
let realDataCheck = false;

function checkForRealData() {
    if(realTodos.length > 0) {
        realDataCheck = true;
    } else {
        realDataCheck = false;
    }
}

window.onload = async function initApp() {
    checkForRealData();

    exampleTodos = await loadExampleTodos();

    toggleExampleTodoList();
    toggleRealTodoList();

    populateTodoList('example');
    if(realTodos.length > 0) {
        populateTodoList('real');
    }
}

async function loadExampleTodos() {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    return response.json();
};

function populateTodoList(decider) {
    let todoRow;
    let todoArray;

    if(decider === "example") {
        todoRow = document.getElementById("exampleTodoRows");
        todoArray = exampleTodos;
    } else if (decider === 'real'){
        todoRow = document.getElementById("realTodoRows");
        todoArray = realTodos;
    }

    todoRow.innerHTML = "";
    todoArray.forEach(todo => {
        
        if(todo.completed) {
            todoRow.innerHTML += `
            <tr>
                <td>
                    <input type="checkbox" onchange='handleTodoCheckboxChange(this)' 
                        value='${todo.completed}' checked>
                </td>
                <td>${todo.title}</td>
            </tr>`
        } else {
            todoRow.innerHTML += `
            <tr>
                <td>
                    <input type="checkbox" onchange='handleTodoCheckboxChange(this)' 
                        value='${todo.completed}'>
                </td>
                <td>${todo.title}</td>
            </tr>`
        }
    });
}

function handleTodoCheckboxChange(checkbox) {
    if(checkbox.checked) {
        !checkbox.checked;
    } else {
        checkbox.checked;
    }
}

function handleExampleCheckboxChange(checkbox) {
    if(checkbox.checked == true){
        exampleCheck = true;
        toggleExampleTodoList();
        toggleRealTodoList();
    }else{
        exampleCheck = false;
        toggleExampleTodoList();
        toggleRealTodoList();
   }
}

function toggleExampleTodoList() {
    if(exampleCheck) {
        document.getElementById("exampleTodoList").style.display = "block";
    } else {
        document.getElementById("exampleTodoList").style.display = "none";
    }
}

function toggleRealTodoList() {
    if(!exampleCheck) {
        if(realDataCheck) {
            document.getElementById("realTodoList").style.display = "block";
        }
        document.getElementById("addTodoArea").style.display = "block";
    } else {
        document.getElementById("realTodoList").style.display = "none";
        document.getElementById("addTodoArea").style.display = "none";
    }
}

function checkEmptyInput(input) {
    if(input.value !== '') {
        document.getElementById("saveTodoButton").disabled = false;
    } else {
        document.getElementById("saveTodoButton").disabled = true;
    }
}

function addTodo() {
    let addTodoInput = document.getElementById("addTodoInput");
    if(addTodoInput.value !== "") {
        let todo = {
            "title": addTodoInput.value,
            "completed": false
        }
        realTodos.push(todo);
        addTodoInput.value = "";
        checkEmptyInput(addTodoInput);
        populateTodoList('real');
        checkForRealData();
        toggleRealTodoList();
    }
}