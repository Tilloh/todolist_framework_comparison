import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealTodoTableComponent } from './real-todo-table.component';

describe('RealTodoTableComponent', () => {
  let component: RealTodoTableComponent;
  let fixture: ComponentFixture<RealTodoTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealTodoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTodoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
