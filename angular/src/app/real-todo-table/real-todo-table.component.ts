import { Component, Input, OnInit } from '@angular/core';
import { Todo } from '../shared/todo';

@Component({
  selector: 'app-real-todo-table',
  templateUrl: './real-todo-table.component.html',
  styleUrls: ['./real-todo-table.component.css']
})
export class RealTodoTableComponent implements OnInit {

  @Input() realTodos!: Todo[];

  constructor() { }

  ngOnInit() {
  }

}
