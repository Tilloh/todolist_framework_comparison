import { Component, Input, OnInit } from '@angular/core';
import { Todo } from '../shared/todo';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  @Input() realTodos!: Todo[];

  newTodo: Todo = new Todo(false, "");

  addTodo() {
      this.realTodos.push(this.newTodo);
      this.newTodo = new Todo(false, "");
  };

  constructor() { }

  ngOnInit() {
  }

}
