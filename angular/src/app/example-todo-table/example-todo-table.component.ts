import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Todo } from '../shared/todo';
import {Observable, of, throwError} from 'rxjs';
import {catchError} from "rxjs/operators";

const EXAMPLE_TODO_API = 'https://jsonplaceholder.typicode.com/todos';

@Component({
  selector: 'app-example-todo-table',
  templateUrl: './example-todo-table.component.html',
  styleUrls: ['./example-todo-table.component.css']
})
export class ExampleTodoTableComponent {

  exampleTodos: string | Todo[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getExampleTodos();
  }

  async getExampleTodos() {
    await this.http.get<Todo[]>(EXAMPLE_TODO_API).pipe(
        catchError(err => "Error fetching example todos. Error: " + err)
      )
      .subscribe(todos => this.exampleTodos = todos);
  }
}
