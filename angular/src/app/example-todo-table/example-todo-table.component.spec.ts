import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleTodoTableComponent } from './example-todo-table.component';

describe('ExampleTodoTableComponent', () => {
  let component: ExampleTodoTableComponent;
  let fixture: ComponentFixture<ExampleTodoTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleTodoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleTodoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
