import { Component } from '@angular/core';
import { Todo } from './shared/todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  realTodos: Todo[] = [
    new Todo(false, "Wäsche waschen"),
    new Todo(true, "Aufräumen"),
    new Todo(true, "Wohnung saugen")
  ];

  exampleCheck = false;
  realDataCheck = this.realTodos.length > 0;
}
