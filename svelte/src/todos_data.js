import { writable } from 'svelte/store';

export const realTodos = writable( [
    {"completed" : false, "title": "Wäsche waschen"},
    {"completed" : true,  "title": "Aufräumen"},
    {"completed" : true,  "title": "Wohnung saugen"}
] );